# 1.0.0

- feat: allow resource tag `alterable-by = user` to alter Terraform managed resources
- feat: (BREAKING) removes `elasticbeanstalk` service to decrease policy size limit to allow alterable tags condition
- chore: update pre-commit schema
- chore: bump pre-commit hooks
- doc: add policy limitations

# 0.0.4

- fix: allows Terraform protection policy to perform basic IAM actions

# 0.0.3

- fix: allows Terraform protection policy to kms:decrypt to read data. Protecting KMS and data should be done elsewhere.
- refactor: use `Desc*` instead of `Describe*` to push further the character limits per policy on terraform protection

# 0.0.2

- fix: allows Terraform protection policy to putObject & deleteObject in S3

# 0.0.1

- fix: makes sure Terraform protection policy still allows to switch role
- refactor: make the `var.tags` merged with local tags in `locals` to prevent mistakes

# 0.0.0

- Initial version
